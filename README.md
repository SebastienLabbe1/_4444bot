# _4444bot

This is a bot which plays : 3d tic tac toe - 4 in a row 3d - puissance 4. (has many names).
It uses the minmax algorithm and many optimisations to look deep enough in the tree of possibilities.

It has been tested against the insane mode on this site : 
https://www.mathsisfun.com/games/foursight-3d-tic-tac-toe.html

Performs very well and most of the time wins in 25 turns.

