#include <iostream>
#include <vector>
#include <bitset>
#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <cassert>

#define UNIT 4

#define MINUSINF (-(1 << (UNIT * 4 - 1)))
#define MINUSTHR (-(1 << (UNIT * 4 - 1)))
#define PLUSINF (1 << (UNIT * 4))
#define PLUSTHR (1 << (UNIT * 4 - 1))

/* 
 * 4x4x4 one slot is 2 bits 00 or 01 empty 10 black 11 white 
 *
 * */


struct Pos {
	int x,y,z;
	Pos(int x=0, int y=0, int z=0){
		this->x = x;
		this->y = y;
		this->z = z;
	}
	void print() {
		std::cout << x << " " << y << " " << z << std::endl;
	}
	int get(int i) {
		return (*((&x) + i));
	}
	bool operator==(const Pos& pos) const {
		return (x == pos.x and y == pos.y and z == pos.z);
	}
	bool operator!=(const Pos& pos) const {
		return (x != pos.x or y != pos.y or z != pos.z);
	}

	struct HashFunction {
		size_t operator()(const Pos& pos) const {
			size_t xHash = std::hash<int>()(pos.x);
			size_t yHash = std::hash<int>()(pos.y) << 1;
			size_t zHash = std::hash<int>()(pos.z) << 2;
			return xHash ^ yHash ^ zHash;
		}
	};
};

struct Line {
	Pos pos, dir;
	Line(Pos pos, Pos dir) {
		this->pos = pos;
		this->dir = dir;
	}
};

Pos lpos(Pos x, Pos y, int a) {
	return Pos( x.x+y.x*a, x.y+y.y*a, x.z+y.z*a);
}

struct Move {
	Pos pos;
	int old_turn, new_turn;
	Move(Pos pos, int old_turn, int new_turn) {
		this->pos = pos;
		this->old_turn = old_turn;
		this->new_turn = new_turn;
	}
};

struct Board {
	int x[4] = {0,0,0,0};
	int turn = 0;
	int score = 0;
	bool is_over = false;
	std::vector<Move> hist, direct_hist;
	bool updated = true;
	std::unordered_set<Pos, Pos::HashFunction> plays;
	Board() {
		reset_plays();
	}
	Board copy() {
		Board b = Board();
		for(int i = 0; i < 4; i ++) b.x[i] = x[i];
		b.turn = turn;
		b.plays = plays;
		b.score = score;
		return b;
	}
	bool operator==(const Board& board) const {
		for(int i = 0; i < 4; i ++) {
			if(board.x[i] != x[i]) return false;
		}
		return true;
	}
	struct HashFunction {
		size_t operator()(const Board& board) const {
			size_t x0 = std::hash<int>()(board.x[0]);
			size_t x1 = std::hash<int>()(board.x[1]) << 1;
			size_t x2 = std::hash<int>()(board.x[2]) << 2;
			size_t x3 = std::hash<int>()(board.x[3]) << 3;
			return x0 ^ x1 ^ x2 ^ x3;
		}
	};
	void reset_plays() {
		//std::unordered_set<Pos, Pos::HashFunction> plays;
		plays.clear();
		for(int i = 0; i < 4; i ++) {
			for(int j = 0; j < 4; j ++) {
				for(int k = 0; k < 4; k ++) {
					plays.insert(Pos(i,j,k));
				}
			}
		}
	}
	int get_pos(Pos pos, int mask = 3) {
		return mask & (x[pos.x] >> (pos.y * 8 + pos.z * 2));
	}
	void set_pos(Pos pos, int v) {
		x[pos.x] = (x[pos.x] & (~(3 << (pos.y * 8 + pos.z * 2)))) | (v & 3) << (pos.y * 8 + pos.z * 2);
	}
	int get_line(Line line) {
		return 
			get_pos(lpos(line.pos,line.dir,0)) << 0 |
			get_pos(lpos(line.pos,line.dir,1)) << 2 |
			get_pos(lpos(line.pos,line.dir,2)) << 4 |
			get_pos(lpos(line.pos,line.dir,3)) << 6 ;
	}
	int count(int x, int m) {
		return 
			int(m == (3 & (x >> 0))) +
			int(m == (3 & (x >> 2))) +
			int(m == (3 & (x >> 4))) +
			int(m == (3 & (x >> 6)));
	}
	int score_line(int x) {
		int c0 = UNIT*count(x,2);
		int c1 = UNIT*count(x,3);
		if (c0 > 0 and c1 > 0) return 0;
		return (1 << c0) - (1 << c1);
	}
	std::vector<Line> get_lines(Pos pos) {
		std::vector<Line> lines {
			Line(Pos(pos.x,pos.y,0), Pos(0,0,1)),
			Line(Pos(pos.x,0,pos.z), Pos(0,1,0)),
			Line(Pos(0,pos.y,pos.z), Pos(1,0,0)),
		};
		bool xy3 = pos.x+pos.y == 3;
		bool yz3 = pos.y+pos.z == 3;
		bool zx3 = pos.z+pos.x == 3;

		bool xy = pos.x == pos.y;
		bool yz = pos.y == pos.z;
		bool zx = pos.z == pos.x;

		Line x3 = Line(Pos(pos.x,3,0), Pos(0,-1,1));
		Line y3 = Line(Pos(0,pos.y,3), Pos(1,0,-1));
		Line z3 = Line(Pos(3,0,pos.z), Pos(-1,1,0));

		Line x = Line(Pos(pos.x,0,0), Pos(0,1,1));
		Line y = Line(Pos(0,pos.y,0), Pos(1,0,1));
		Line z = Line(Pos(0,0,pos.z), Pos(1,1,0));

		Line _000 = Line(Pos(0,0,0), Pos(1,1,1));
		Line _003 = Line(Pos(0,0,3), Pos(1,1,-1));
		Line _033 = Line(Pos(0,3,3), Pos(1,-1,-1));
		Line _030 = Line(Pos(0,3,0), Pos(1,-1,1));

		if (xy3) { lines.push_back(z3); } 
		else if (xy) { lines.push_back(z); }

		if (yz3) { lines.push_back(x3); } 
		else if (yz) { lines.push_back(x); }

		if (zx3) { lines.push_back(y3); } 
		else if (zx) { lines.push_back(y); }

		if (xy3) { 
			if (yz3) { lines.push_back(_030); } 
			else if (yz) { lines.push_back(_033); }
		} else if (xy) { 
			if (yz3) { lines.push_back(_003); } 
			else if (yz) { lines.push_back(_000); }
		}
		return lines;
	}

	int score_pos(Pos pos) {
		auto lines = get_lines(pos);
		int score = 0;
		for (auto line : lines) {
			int x = get_line(line);
			int s = score_line(x);
			score += s;
		}
		return score;
	}

	int score_board() {
		std::vector<Line> lines {
			Line(Pos(0,0,0),Pos(1,1,1)),
			Line(Pos(0,3,0),Pos(1,-1,1)),
			Line(Pos(0,3,3),Pos(1,-1,-1)),
			Line(Pos(0,0,3),Pos(1,1,-1)),
		};
		for(int i = 0; i < 4; i ++) {
			for(int j = 0; j < 4; j ++) {
				lines.push_back(Line(Pos(i,j,0),Pos(0,0,1)));
				lines.push_back(Line(Pos(0,i,j),Pos(1,0,0)));
				lines.push_back(Line(Pos(j,0,i),Pos(0,1,0)));
			}
		}
		for(int i = 0; i < 4; i ++) {
			lines.push_back(Line(Pos(i,0,0),Pos(0,1,1)));
			lines.push_back(Line(Pos(0,i,0),Pos(1,0,1)));
			lines.push_back(Line(Pos(0,0,i),Pos(1,1,0)));
			lines.push_back(Line(Pos(i,3,0),Pos(0,-1,1)));
			lines.push_back(Line(Pos(0,i,3),Pos(1,0,-1)));
			lines.push_back(Line(Pos(3,0,i),Pos(-1,1,0)));
		}
		int score = 0;
		for(auto l : lines){
			int line = get_line(l);
			int tscore = score_line(line);
			if(abs(tscore) == PLUSINF) { return tscore; }
			score += tscore;
		}
		return score;
	}
	void set_line(Line line) {
		set_pos(lpos(line.pos,line.dir,0),1);
		set_pos(lpos(line.pos,line.dir,1),1);
		set_pos(lpos(line.pos,line.dir,2),1);
		set_pos(lpos(line.pos,line.dir,3),1);
	}
	void update() {
		if(updated) return;
		std::cout << "update" << std::endl;
		Move move = hist.back();
		set_pos(move.pos, move.old_turn);
		int prev_score = score_pos(move.pos);
		set_pos(move.pos, move.new_turn);
		int new_score = score_pos(move.pos);
		score += new_score - prev_score;
		if (abs(score) > PLUSTHR or plays.empty()) is_over = true;
		if (!move.new_turn) is_over = false;
		turn = 1-turn;
		updated = true;
	}
	void play_pos(Pos pos) { 
		std::cout << "playpos" << std::endl;
		assert(updated);
		Move move = Move(pos, get_pos(pos), turn + 2);
		set_pos(move.pos, move.new_turn);
		plays.erase(move.pos);
		hist.push_back(move);
		direct_hist.push_back(move);
		updated = false;
	}
	void undo() {
		std::cout << "undo" << std::endl;
		if (!updated) {
			Move move = direct_hist.back();
			set_pos(move.pos, move.old_turn);
			plays.insert(move.pos);
			hist.pop_back();
			direct_hist.pop_back();
			updated = true;
			return;
		}
		assert(updated);
		Move move = direct_hist.back();
		direct_hist.pop_back();
		plays.insert(move.pos);
		int t = move.old_turn;
		move.old_turn = move.new_turn;
		move.new_turn = t;
		hist.push_back(move);
		updated = false;
		update();
	}
	/*
	void play_pos(Pos pos) { 
		//pos.z = 0;
		//int line = get_line(Line(pos,Pos(0,0,1)));
		//int c = count(line,2) + count(line,3);
		//pos.z = c;

		hist.push_back(pos);
		plays.erase(pos);

		turn = 1-turn;

		int prev_score = score_pos(pos);
		set_pos(pos, turn + 2);
		int new_score = score_pos(pos);
		score += new_score - prev_score;

		if (abs(score) > PLUSTHR or plays.empty()) { is_over = true; }
	}
	void undo() {
		Pos pos = hist.back();
		hist.pop_back();
		plays.insert(pos);
		turn = 1-turn;

		int prev_score = score_pos(pos);
		set_pos(pos, 0);
		int new_score = score_pos(pos);
		score += new_score - prev_score;

		is_over = false;
	}
	*/
	void print() {
		if(!updated) update();
		std::cout << "Board : " << std::endl;
		for(int i = 0; i < 4; i ++) {
			for(int j = 0; j < 4; j ++) {
				for(int k = 0; k < 4; k ++) {
					int c = get_pos(Pos(i,j,k));
					std::cout << ((c<2) ? ((c==1) ? "x" : ".") : ((c-2) ? "3" : "2")) << "  ";
					//std::cout << std::bitset<8>(x[i] >> (8*j)) << "   ";
				}
				std::cout << "   " << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << "Turn : " << turn << std::endl;
		std::cout << "Current board score : " << score << std::endl;
		//std::cout << "Full score : " << score_board() << std::endl;
		std::cout << "Number of possible moves : " << plays.size() << std::endl;
		std::cout << "Is the game over : " << is_over << std::endl;
		//std::cout << "Plays empty : " << plays.empty() << std::endl;
		std::cout << "Moves played : " << hist.size() << std::endl;
	}
	bool is_equal(Board b) {
		for(int i = 0; i < 4; i ++) {
			if(x[i] != b.x[i]) return false;
		}
		//if (turn != b.turn) return false;
		return true;
	}
};

struct State {
	int depth, score;
	Pos play;
	State(int depth=0, int score=0, Pos play=Pos(-1,-1,-1)) {
		this->depth = depth;
		this->score = score;
		this->play = play;
	}
	
	bool operator < (const State& state) const {
		return score < state.score;
	}
};

struct AlphaBeta {

	Board board;
	std::unordered_map<Board, State, Board::HashFunction> cache;
	std::vector<std::unordered_map<Pos, int, Pos::HashFunction>> order;
	std::chrono::time_point<std::chrono::system_clock> start;
	double max_time;
	int count;
	AlphaBeta() {
		order.resize(64);
	}

	time_t get_time() {
		auto time = (std::chrono::system_clock::now() - start);
		return time.count();
	}

	bool stop() {
		return get_time() > max_time*10e8;
	}

	State alphabeta2(double max_time) {
		count = 0;
		this->max_time = max_time;
		start = std::chrono::system_clock::now();
		int depth = 1;
		State fin,temp;
		while(1) {
			temp = alphabeta(depth++, MINUSTHR, PLUSTHR, true);
			if (depth > 2 or stop()) break;
			std::cout << " hehhehe " << std::endl;
			fin = temp;
		}
		std::cout << "cps : " << count * 10e8 / get_time() << std::endl;
		std::cout << "time : " << get_time()/10e8 << std::endl;
		return fin;
	}

	State score(Pos play, int depth, int alpha, int beta) {
		State fin;
		if (cache.find(board)!=cache.end()) {
			fin = cache[board];
			if (fin.depth >= depth) return fin;
		}
		if (stop()) return fin;
		if (board.is_over) fin.depth = -2;
		if (!stop() and depth and !board.is_over and fin.depth < depth) {
			cache[board] = fin;
		}
		return fin;
	}

	State alphabeta(int depth, int alpha = MINUSTHR, int beta = PLUSTHR, bool is_main = false) {
		std::cout << " call with depth " << depth << std::endl;
		count ++;
		State fin;

		if (cache.find(board)!=cache.end()) {
			fin = cache[board];
			if(fin.depth >= depth) {
				std::cout << " cached " << fin.score << std::endl;
				return fin;
			}
		}

		board.update();
		//assert(!depth or !((board.turn + depth)%2));
		if (!depth or board.is_over or stop()) {
			std::cout << !depth << board.is_over << stop() << std::endl;
			fin.score = board.score;
			cache[board] = fin;
			return fin;
		}
		int tturn = board.turn;

		fin.depth = depth;
		std::vector<State> node_plays;
		for (auto play : board.plays) node_plays.push_back(State(-1,0,play));
		if (depth > 1) {
			int c = 2 * board.turn -1;
			for (int i = 0; i < board.plays.size(); i ++) {
				board.play_pos(node_plays[i].play);
				State state = alphabeta(0);
				node_plays[i].score = state.score * c;
				board.undo();
			}
			std::sort(node_plays.begin(), node_plays.end());
		}

		fin.score = (!board.turn) ? PLUSINF : MINUSINF;
		for (State state : node_plays) {
			board.play_pos(state.play);
			State res = alphabeta(depth-1,alpha,beta);
			board.undo();
			//board.update();
			if(!board.turn) {
				if (res.score < fin.score) { fin.play = state.play; }
				fin.score = std::min(res.score,fin.score);
				if (fin.score <= alpha) { break; }
				beta = std::min(beta,fin.score);
			} else {
				if (res.score > fin.score) { fin.play = state.play; }
				fin.score = std::max(res.score,fin.score);
				if (fin.score >= beta) { break; }
				alpha = std::max(alpha,fin.score);
			}
		}
		cache[board] = fin;
		return fin;
	}
};

Pos get_input() {
	Pos pos;
	std::cout << "Input pos :  " << std::endl;
	std::cin >> pos.x >> pos.y >> pos.z;
	return pos;
}

void test() {
	Board b = Board();
	b.print();
	b.play_pos(Pos());
	b.print();
	b.play_pos(Pos(1,0,0));
	b.update();
	b.print();
	b.undo();
	b.print();
	b.undo();
	b.print();
}

int main() {
	//test();
	//return 0;
	while(1) {
		std::cout << "Next Game" << std::endl;
		AlphaBeta ab = AlphaBeta();
		while(!ab.board.is_over) {
			std::cout << std::endl << std::endl << std::endl << std::endl;
			std::cout << std::endl << std::endl << std::endl << std::endl;
			std::cout << std::endl << std::endl << std::endl << std::endl;
			std::cout << "Next Move" << std::endl;
			ab.board.print();
			Pos play;
			if(ab.board.turn) { 
				play = get_input();
			} else {
				auto res = ab.alphabeta2(3);
				std::cout << "Depth reached " << res.depth << std::endl;
				std::cout << "predicted board score : " << res.score << " move :  ";
				std::cout << "move : ";
				res.play.print();
				play = res.play;
			}
			ab.board.play_pos(play);
			ab.board.update();
		}
		std::cout << "End Board" << std::endl;
		ab.board.print();
		std::cout << "Turns played : " << ab.board.hist.size() << std::endl;
	}
	return 0;
}
